Image location.

wget なんかで一括取得した方が良いかもねぇ. 

- Bullseye.png
  - https://static.wikia.nocookie.net/pixar/images/1/11/Ddhg6tl-5c1bedb5-645a-41d9-86ed-08ba6eb1daa8.png/revision/latest?cb=20210203004509

- Bookworm.png
  - https://static.wikia.nocookie.net/pixar/images/d/db/311186C6-D83E-494B-9767-91A768E4AD53.png/revision/latest?cb=20181206152047

- Trixei
  - https://static.wikia.nocookie.net/pixar/images/7/7e/0AD44E98-A7C1-4F8F-9C68-7F3D26A48DD1.png/revision/latest?cb=20221121131820

- Forkie
  - https://static.wikia.nocookie.net/pixar/images/4/4c/Fork.png/revision/latest?cb=20210210225531

- Raspberry Pi logo:
  - https://www.raspberrypi.com/app/uploads/2022/02/COLOUR-Raspberry-Pi-Symbol-Registered.png

- Debconf 23 Group Photo
  - https://wiki.debian.org/DebConf/23/Photos?action=AttachFile&do=view&target=debconf23_group.jpg
