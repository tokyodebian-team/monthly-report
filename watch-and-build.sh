#!/bin/bash

#
# Simple utility script to watch update and build instantly.
#
# Usage:
#
# $ cd 2023/
# $ ../watch-and-build.sh debianmeetingresume2023-fuyu.pdf &
#

PDF=$1
if [ ! -f "$PDF" ]; then
    echo "ERROR: Assume that $PDF was already built."
    exit 1
fi

BASE=${PDF%.pdf}
TEX="${BASE}.tex"
if [ ! -f "$TEX" ]; then
    echo "ERROR: Assume source $TEX exist."
    exit 1
fi
while true; do
    inotifywait $TEX
    make $PDF
done
