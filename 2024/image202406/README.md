画像取得元
-----------

- UbuntuCoF.png: Public Domain? 
  - https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/UbuntuCoF.svg/480px-UbuntuCoF.svg.png
- COLOUR-Raspberry-Pi-Symbol-Registered.png: 
  - https://www.raspberrypi.com/app/uploads/2022/02/COLOUR-Raspberry-Pi-Symbol-Registered.png
- kali-dragon-icon-svg:
  - https://www.kali.org/images/kali-dragon-icon.svg
- Debconf 23 Group Photo
  - https://wiki.debian.org/DebConf/23/Photos?action=AttachFile&do=view&target=debconf23_group.jpg
- Debconf 24 Trademark
  - https://debconf24.debconf.org/static/img/jumbotron-image.1920e0770dc6.svg
- Bullseye.png
  - https://static.wikia.nocookie.net/pixar/images/1/11/Ddhg6tl-5c1bedb5-645a-41d9-86ed-08ba6eb1daa8.png/revision/latest?cb=20210203004509
- Bookworm.png
  - https://static.wikia.nocookie.net/pixar/images/d/db/311186C6-D83E-494B-9767-91A768E4AD53.png/revision/latest?cb=20181206152047
- Trixei
  - https://static.wikia.nocookie.net/pixar/images/7/7e/0AD44E98-A7C1-4F8F-9C68-7F3D26A48DD1.png/revision/latest?cb=20221121131820
- Forkie
  - https://static.wikia.nocookie.net/pixar/images/4/4c/Fork.png/revision/latest?cb=20210210225531


Git に登録するには
----

```
convert -size 512x512 -bordercolor "#000000" -border 2x2 -background white -pointsize 35 -fill black -gravity South label:"Forky image"
```

みたいな感じでダミー画像生成しておくのが良い. Makefile でも描いておきますかね. うーん.
